from django.contrib import admin
from django.urls import path,include
from django.contrib.auth.views import login, logout_then_login, password_reset, password_reset_done, \
										password_reset_confirm, password_reset_complete

urlpatterns = [
    path('accounts/login/',login, {'template_name':'index.html'}, name='login'),
    path('admin/', admin.site.urls),
    path('mascota/',include('apps.mascota.urls'),name='mascota'),
    path('adopcion/',include('apps.adopcion.urls'),name='adopcion'),
    path('usuario/',include('apps.usuario.urls'),name='usuario'),
    path('logout/', logout_then_login, name = 'logout'),

    path('reset/password_reset/', password_reset, 
    	{'template_name':'registration/password_reset_form.html',
    	 'email_template_name': 'registration/password_reset_email.html'}, 
    	 name = 'password_reset'),
    path('reset/password_reset_done/', password_reset_done, 
    	{'template_name':'registration/password_reset_done.html'},
    	name = 'password_reset_done'),
    path('reset/<slug:uidb64>/<slug:token>/', password_reset_confirm, 
    	{'template_name': 'registration/password_reset_confirm.html'},
    	name = 'password_reset_confirm'),
    path('reset/done/', password_reset_complete, 
    	{'template_name': 'registration/password_reset_complete.html'},
    	name= 'password_reset_complete'),
]
